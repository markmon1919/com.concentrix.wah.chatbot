﻿namespace com.concentrix.wah.chatbot.ui
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.statBox = new System.Windows.Forms.TextBox();
            this.statLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 22);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(116, 22);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // statBox
            // 
            this.statBox.BackColor = System.Drawing.SystemColors.WindowText;
            this.statBox.ForeColor = System.Drawing.Color.Lime;
            this.statBox.Location = new System.Drawing.Point(12, 69);
            this.statBox.Multiline = true;
            this.statBox.Name = "statBox";
            this.statBox.Size = new System.Drawing.Size(179, 157);
            this.statBox.TabIndex = 2;
            // 
            // statLabel
            // 
            this.statLabel.AutoSize = true;
            this.statLabel.ForeColor = System.Drawing.Color.Gold;
            this.statLabel.Location = new System.Drawing.Point(13, 52);
            this.statLabel.Name = "statLabel";
            this.statLabel.Size = new System.Drawing.Size(40, 13);
            this.statLabel.TabIndex = 3;
            this.statLabel.Text = "Status:";
            this.statLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(204, 238);
            this.Controls.Add(this.statLabel);
            this.Controls.Add(this.statBox);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "AshleyChatBot";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox statBox;
        private System.Windows.Forms.Label statLabel;
    }
}

